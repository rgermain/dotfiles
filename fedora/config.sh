#!/bin/bash

cd $(dirname "$0")
color_blue="../common/linked/scripts/dev/color/color-blue"
source ../utils/link.sh

echo -e "\n[install config $($color_blue system)]"

LINKED="$(pwd)/linked"

force-link-config "dnf.conf" "$LINKED/dnf.conf" "/etc/dnf/dnf.conf"

