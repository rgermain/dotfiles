#!/bin/bash

cd $(dirname "$0")
color_blue="../common/linked/scripts/dev/color/color-blue"

echo -e "\n[install for $($color_blue fedora)]"

./packages.sh
./config.sh
../common/install.sh
../common/gnome.sh "dnf install -y" "dnf autoremove -y"
