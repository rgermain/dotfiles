#!/bin/bash

cd $(dirname $0)
color_blue="../common/linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue install packages system)]"

apps=(
    # other
    flatpak

    ulauncher
    ImageMagick
    codium
    dconf
    dnf-plugin-system-upgrade

    # ops
    terminator
    git
    zsh
    tmux
    vim
    zsh
    curl
    sudo
    tree
	ascii
    g++
    gcc
    python3
    python3-pip
    ruby
    nodejs
    rust
    cargo
    openssh
    sshfs
    ftp
    xclip

    # tools
    cmake
    valgrind
    net-tools
    powerline
    adb
    htop
    inotify-tools

    # lib
    jq
    glib2-devel
    ffmpeg-devel
	ffmpeg
	ffmpegthumbnailer
    gnome-directory-thumbnailer
	libavif-tools
	libavif
    libasan
    inotify-tools

    libevdev-devel
    libudev-devel
    libconfig-devel

	# tor
	macchanger
)

remove_apps=(
    firefox
)

echo "add rpmfusion repo..."
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

echo "add codium repo..."
sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo

# remvove docker
 sudo dnf remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine
sudo dnf -y install dnf-plugins-core
sudo dnf config-manager \
    --add-repo \
    https://download.docker.com/linux/fedora/docker-ce.repo
sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin

echo "add repo librewolf..."
sudo dnf config-manager --add-repo https://rpm.librewolf.net/librewolf-repo.repo

echo "update app"
sudo dnf update -y

echo "install app..."
sudo dnf install -y --allowerasing --skip-broken "${apps[@]}"

echo "config for docker..."
sudo systemctl enable docker
sudo systemctl start docker

echo "remove app..."
sudo dnf autoremove -y "${remove_apps[@]}"

sudo dnf clean all
