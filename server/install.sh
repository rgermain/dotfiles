#!/usr/bin/env bash

cd $(dirname "$0")

color_blue="../common/linked/scripts/dev/color/color-blue"
echo -e "\n[install for $($color_blue server)]"

./packages.sh
./config.sh