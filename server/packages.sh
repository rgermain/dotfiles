#!/usr/bin/env bash

cd $(dirname $0)
color_blue="../common/linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue install packages system)]"

app=(
    #ops
    git
    zsh
    tmux
    vim
    zsh
    curl
    sudo
    tree
    docker
    docker-compose
    ssh
    sshfs
    sftp

    # tools
    htop
    

    # language
    python3

    # lib
    jq

    # secu
    iptables
    fail2ban
    
)

echo "install ${apps[@]}..."
sudo apt install -y ${app[@]}


# docker
sudo systemctl enable docker
# add current user to docker group
sudo groupadd docker
sudo usermod -aG docker $USER

echo "upgrade pip..."
sudo python3 -m pip install --upgrade pip

# fail2ban
sudo systemctl enable fail2ban