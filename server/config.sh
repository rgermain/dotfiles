#!/usr/bin/env bash

cd $(dirname $0)
source ../utils/link.sh

color_blue="../common/linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue intisalize config)]"

echo "create workspaces folder..."
mkdir -p $BASE_DIR_PATH
mkdir -p $BIN_PATH
mkdir -p $WORKSPACES_PATH
LINKED="$(dirname $(pwd))/common/linked"


echo "install oh my zsh..."
curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh > /tmp/zsh.sh
bash -f /tmp/zsh.sh --unattended
rm -rf /tmp/zsh.sh
force-link-config "zshrc" "$LINKED/.zshrc" "$HOME/.zshrc"

echo "link zshrc..."
force-link-config "$LINKED/.zshrc" "$HOME/.zshrc"
force-link-config "$LINKED/.extra.zshrc" "$HOME/.extra.zshrc"

echo "link scripts..."
force-link-config "$LINKED/scripts" $SCRIPTS_PATH

force-link-config "vimrc" "$LINKED/nvim/init.vim" "$HOME/.vimrc"
echo "install vim plugin..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
which vim && vim -c "PlugInstall" -c "exit" -c "exit"

echo "chmod ssh..."
mkdir -p ~/.ssh ~/.cache/ssh
chmod 700 ~/.ssh ~/.cache/ssh

echo "generate ssh key..."
ssh-keygen -f "$HOME/.ssh/github" -q -N ""
ssh-keygen -f "$HOME/.ssh/gitlab" -q -N ""

force-link-config "git" "$LINKED/.gitconfig" "$HOME/.gitconfig"

force-link-config "scripts" "$LINKED/scripts" $SCRIPTS_PATH
chmod +x -R $SCRIPTS_PATH