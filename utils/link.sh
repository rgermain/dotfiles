#!/bin/bash

export BASE_DIR_PATH="$HOME/.myconfig"
export APP_PATH="$BASE_DIR_PATH/app"
export BIN_PATH="$BASE_DIR_PATH/bin"
export SCRIPTS_PATH="$BASE_DIR_PATH/scripts"

function link-config () {
    echo -e "link $1..."
    from=$2
    to=$3
    if [[ $4 == "-y" ]]; then
        rm -rf $to
    fi
    mkdir -p "$(dirname "$to")"
    ln -s "$from" "$to"
}


function force-link-config () {
    link-config "$1" "$2" "$3" -y
}
