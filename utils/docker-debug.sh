#!/usr/bin/env bash

echo "download docker-debug..."
VERSION=$(curl -w '%{url_effective}' -I -L -s -S https://github.com/zeromake/docker-debug/releases/latest -o /dev/null | awk -F/ '{print $NF}')
curl -Lo docker-debug https://github.com/zeromake/docker-debug/releases/download/${VERSION}/docker-debug-linux-amd64
mv docker-debug-linux-amd64 docker-debug 2>/dev/null
chmod +x ./docker-debug
mv docker-debug /usr/local/bin/
rm -f docker-debug

echo "change docker storage-driver to overlay2..."
content=$(cat /etc/docker/daemon.json 2>/dev/null || echo "{}")
echo "$content" | jq '. += {"storage-driver": "overlay2"}' > /etc/docker/daemon.json

echo "restart docker daemon..."
systemctl restart docker

