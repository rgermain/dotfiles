#!/usr/bin/env bash

echo "install k8s krew..."
{
    set -x
    cd "$(mktemp -d)" &&
        OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
        ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
        KREW="krew-${OS}_${ARCH}" &&
        curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
        tar zxvf "${KREW}.tar.gz" &&
        ./"${KREW}" install krew
}

echo "install k8s plugins..."
kubectl krew install ctx
kubectl krew install ns

echo "kubernetes synlinks..."
which kubectl-ns &>/dev/null && sudo ln -s "$(which kubectl-ns)" /usr/local/bin/kubens
which kubectl-oidc_login &>/dev/null && sudo ln -s "$(which kubectl-oidc_login)" /usr/local/bin/kubelogin
which kubectl-ctx &>/dev/null && sudo ln -s "$(which kubectl-ctx)" /usr/local/bin/kubectx

echo "completions ctx/ns..."
mkdir -p ~/.oh-my-zsh/custom/completions
chmod -R 755 ~/.oh-my-zsh/custom/completions
ln -s /opt/kubectx/completion/_kubectx.zsh ~/.oh-my-zsh/custom/completions/_kubectx.zsh
ln -s /opt/kubectx/completion/_kubens.zsh ~/.oh-my-zsh/custom/completions/_kubens.zsh

# clear pb
rm -f ~/.kube/cache/oidc-login
