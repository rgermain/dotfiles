# ruff: noqa: F401,F403, E402
import functools
import logging
import sys

__root_logger = logging.getLogger()
__root_stream = logging.StreamHandler(stream=sys.stdout)
__root_stream.setFormatter(logging.Formatter("[%(name)s::%(levelname)s] %(message)s"))
__root_logger.addHandler(__root_stream)


def __set_level(level=logging.INFO):
    __root_logger.setLevel(level)
    __root_stream.setLevel(level)


logging.disable = functools.partial(__set_level, 10000000000000000)
logging.enable = __set_level
logging.disable()

import ast
import asyncio
import bisect
import collections
import contextlib
import copy
import csv
import datetime
import functools
import glob
import hashlib
import heapq
import inspect
import io
import itertools
import json
import math
import multiprocessing
import os
import pathlib
import platform
import pprint as pp
import queue
import random
import re
import select
import shlex
import shutil
import signal
import socket
import ssl
import stat
import string
import subprocess
import threading
import time
import typing
from fnmatch import fnmatch
from os import getenv
from pathlib import Path
from urllib.request import urljoin, urlopen

debug = pp.PrettyPrinter(indent=4, compact=False, underscore_numbers=True).pprint


@contextlib.contextmanager
def __safeimport__():
    try:
        yield
    except (ModuleNotFoundError, ImportError):
        pass


with __safeimport__():
    import requests

with __safeimport__():
    import httpx

with __safeimport__():
    import regex as re

with __safeimport__():
    import docker

with __safeimport__():
    import tomllib

with __safeimport__():
    import panda as pd

with __safeimport__():
    import polars as pl

with __safeimport__():
    import tensorflow as tf

with __safeimport__():
    import numpy as np

print(f"-- load {len(locals())!r} modules {getenv('PYTHONSTARTUP')} ---")
