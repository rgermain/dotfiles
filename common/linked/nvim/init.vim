set noswapfile
set nobackup nowritebackup
set nowritebackup
set noundofile
set nocompatible
set noerrorbells
set encoding=utf-8
set tabstop=4
set mouse=a
set mouse=v
set laststatus=2
set shell=zsh
set termguicolors
set relativenumber
set hidden
set cmdheight=2
set updatetime=300
syntax on
set ttyfast
set incsearch
set hlsearch
set showmatch

set showmode
set showcmd
set cursorline
set autoindent

cnoreabbrev w!! w !sudo tee % >/dev/null
cnoreabbrev wq!! wq !sudo tee % >/dev/null

call plug#begin('~/.vim/plugged')

"Plug 'ludovicchabant/vim-gutentags'

Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-dispatch'

Plug 'sheerun/vim-polyglot'
Plug 'joshdick/onedark.vim'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"Plug 'lukas-reineke/indent-blankline.nvim'

Plug 'akinsho/bufferline.nvim'

call plug#end()

" config
colorscheme onedark
let g:airline_theme = 'onedark'


let g:gruvbox_guisp_fallback = 'bg'

