import argparse
import json
import os
from functools import cache

import docker

DOCKER_CONTAINER = os.getenv("NETSHOOT_DOCKER")
DOCKER_SOCKET = "unix://var/run/docker.sock"

client = docker.DockerClient(base_url=DOCKER_SOCKET)
apiclient = docker.APIClient(base_url=DOCKER_SOCKET)
container = apiclient.inspect_container(DOCKER_CONTAINER)


def workdir(*path):
    return os.path.join(container["Config"]["WorkingDir"], *path)


@cache
def builenv():
    env = container["Config"]["Env"]
    final = {}
    for content in env:
        k, v = content.split("=", 1)
        final[k] = v
    return final


def inspect(flags):
    print(json.dumps(container, indent=4))


def entrypoint(flags):
    env = builenv()
    entrypoints = container["Config"]["Entrypoint"]
    cmds = container["Config"]["Cmd"]

    print(f"From CMD in Dockerfile:\n [{', '.join(repr(v) for v in cmds)}]\n")
    print(f"From ENTRYPOINT in Dockerfile:\n [{', '.join(repr(v) for v in entrypoints)}]\n")
    print("The container has been started with following command:\n")
    path = " ".join(entrypoints)
    args = " ".join(cmds)
    print(f"{path} {args}\n")

    print(f"path: {path}")
    print(f"args: {args}")
    print(f"PATH: {env['PATH']}")
    print(f"cwd: {container['Config']['WorkingDir']}")


def info(flags):
    datas = [
        ("Started", container["State"]["StartedAt"]),
        ("Platform", container["Platform"]),
        ("ExposedPorts", ", ".join(container["Config"]["ExposedPorts"].keys())),
        (
            "Networks",
            repr(
                ", ".join(
                    [
                        name
                        for name, config in container["NetworkSettings"]["Networks"].items()
                        if config.get("IPAddress")
                    ]
                )
            ),
        ),
        ("Image", repr(container["Image"])),
        ("Entrypoint", repr(", ".join(container["Config"]["Entrypoint"]))),
        ("Cmd", repr(", ".join(container["Config"]["Cmd"]))),
    ]
    print(f"--- {DOCKER_CONTAINER} ---")
    pad = 0
    for k, _ in datas:
        pad = max(len(k), pad)
    pad += 1
    for k, v in datas:
        print(f"{(k + ':').ljust(pad)}   {v}")


def logs(flags):
    container = client.containers.get(DOCKER_CONTAINER)
    try:
        for log in container.logs(stream=True):
            print(log.decode("utf-8"))
    except KeyboardInterrupt:
        pass


def main():
    parser = argparse.ArgumentParser("docker debug")
    command = parser.add_subparsers(
        dest="command", description="docker command to interact with docker api", required=True
    )

    command.add_parser("entrypoint", help="ENTRYPOINT file")

    command.add_parser("info", help="Show minimal info from container")
    command.add_parser("inspect", help="Show inspect info from container")
    command.add_parser("logs", help="Show logs")

    flags = parser.parse_args()

    if flags.command == "entrypoint":
        entrypoint(flags)
    elif flags.command == "info":
        info(flags)
    elif flags.command == "logs":
        logs(flags)
    elif flags.command == "inspect":
        inspect(flags)


if __name__ == "__main__":
    main()
