#!/bin/bash

cd $(dirname $0)
color_blue="./linked/scripts/dev/color/color-blue"

source ../utils/link.sh

echo -e "\n[$($color_blue install system config)]"

SYS_PATH="system"
PREFIX="$(pwd)/$SYS_PATH"

echo "install grub config..."
sudo cp -r "$PREFIX/grub/grub" "/etc/default/grub"
sudo cp -r "$PREFIX/grub/themes" "/boot/grub2/themes"

function has_command() {
  command -v "$1" > /dev/null
}

echo "update grub..."
if has_command update-grub; then
    sudo update-grub
elif has_command grub-mkconfig; then
    sudo grub-mkconfig -o /boot/grub/grub.cfg
elif has_command grub2-mkconfig; then
    if has_command zypper; then
        sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    elif has_command dnf; then
        sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
    else
        echo "not find any grub command"
    fi
else
    echo "not find any grub command"
fi

if has_command update-initramfs; then
    sudo update-initramfs -u -k all
elif has_command dracut; then
    sudo dracut --regenerate-all --force
else
    echo "initramfs command not found..."
fi
