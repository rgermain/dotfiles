#!/bin/bash

#*******************************#
#          python
#*******************************#


cd $(dirname $0)
color_blue="../linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue install php packages)]"

cd /tmp
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod +x /usr/local/bin/composer
