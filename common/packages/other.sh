#!/bin/bash

#*******************************
#          flatpak
#*******************************

cd $(dirname "$0")
color_blue="../linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue install utils packages)]"


echo "install arduino-cli..."
cd $(dirname $BIN_PATH)
curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh  | bash

echo "install gdb enhanced features..."
bash -c "$(curl -fsSL http://gef.blah.cat/sh)"

echo "install androidsdk..."
wget "https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip"
tar xf "commandlinetools-linux-7583922_latest.zip"
rm -rf "commandlinetools-linux-7583922_latest.zip"

echo "install repo cli..."
mkdir -p ~/bin
REPO=$(mktemp /tmp/repo.XXXXXXXXX)
curl -o ${REPO} https://storage.googleapis.com/git-repo-downloads/repo
gpg --recv-key 8BB9AD793E8E6153AF0F9A4416530D5E920F5C65
curl -s https://storage.googleapis.com/git-repo-downloads/repo.asc | gpg --verify - ${REPO} && sudo install -m 755 ${REPO} ~/bin/repo

echo "insfall z..."
git clone https://github.com/rupa/z.git $BIN_PATH/z
