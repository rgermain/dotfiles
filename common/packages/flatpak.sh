#!/bin/bash

#*******************************
#          flatpak
#*******************************

cd $(dirname $0)
color_blue="../linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue install flatpak packages)]"


echo "add flathub repo..."
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo "install apps..."
flatpak install --or-update -y $(cat flatpak.txt)