#!/bin/bash

#*******************************#
#          python
#*******************************#


cd $(dirname $0)
color_blue="../linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue install python packages)]"

lib=(
    regex
    requests
    httpx
    Xlib
    pip-review
    build
    twine
    bpython
    docker

    black
    pytest
    pytest-cov

    ytmdl
    spotdl
    virtualenvwrapper
    ruff
    pyenv
    poetry
)

echo "upgrade pip..."
python3 -m pip install --upgrade pip

echo "install lib with pip..."
python3 -m pip install "${lib[@]}"

echo "install ptemtyper..."
python3 -m pip install git+https://github.com/kraanzu/termtyper.git

echo "install uv..."
curl -LsSf https://astral.sh/uv/install.sh | sh