#!/bin/bash

#*******************************
#          javascript
#*******************************

cd $(dirname $0)
color_blue="../linked/scripts/dev/color/color-blue"

echo "install requirements..."
sudo dnf install bash curl file git unzip which xz zip mesa-libGLU clang cmake ninja-build pkg-config gtk3-devel

mkdir "$BASE_DIR_PATH"
cd "$BASE_DIR_PATH"

echo "dowload archive..."
wget https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.22.1-stable.tar.xz
tar xvf flutter_linux_*-stable.tar.xz

echo "remove archive..."
rm -rf flutter_linux_*-stable.tar.xz

echo "disable analytics..."
dart --disable-analytics
flutter config --disable-analytics