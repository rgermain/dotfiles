#!/bin/bash

#*******************************
#          javascript
#*******************************

cd $(dirname $0)
color_blue="../linked/scripts/dev/color/color-blue"

echo "install nvm..."
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash

nvm install --lts


yarn=$(which yarn)
if [ $? ]; then
    yarn=$(which yarnpkg)
fi

YARN_PATH="$HOME/.config/node/global"
npm config set --home prefix "$YARN_PATH"
npm config set --home enableTelemetry 0
npm config set --home telemetryInterval null

json=$(cat ~/.config/node/global/package.json | jq  '.dependencies')
keys=($(echo "$json" | jq 'keys[]' | tr -d "\""))
value=($(echo "$json" | jq 'values[]' | tr -d "\""))

cd ~/.config/node/global/
npm install -G
