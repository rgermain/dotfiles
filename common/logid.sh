#!/bin/bash

cd $(dirname $0)

echo "copy..."
sudo cp ./system/services/logidfix.sh /usr/local/bin/logidfix.sh

echo "chmod..."
sudo chmod +x /usr/local/bin/logidfix.sh

echo "service..."
sudo cp ./system/services/logidfix.service /etc/systemd/system/logidfix.service

echo "create udev rules..."
sudo cp ./system/services/99-logidfix.rules /etc/udev/rules.d/99-logidfix.rules

echo "restart systemd..."
sudo systemctl daemon-reload
sudo udevadm control --reload-rules

echo "enable service..."
sudo systemctl enable --now logidfix.service