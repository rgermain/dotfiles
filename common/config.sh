#!/bin/bash

#*******************************
#          config
#*******************************

cd $(dirname $0)
source ../utils/link.sh

color_blue="linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue intisalize config)]"

LINKED="$(pwd)/linked"

echo "create workspaces folder..."
mkdir -p $BASE_DIR_PATH
mkdir -p $APP_PATH
mkdir -p $BIN_PATH

echo "install oh my zsh..."
rm -rf ~/.oh-my-zsh
curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh > /tmp/zsh.sh
bash -f /tmp/zsh.sh --unattended
rm -rf /tmp/zsh.sh

PLUGINS_ZSH=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins
echo "install plugins zsh..."
mkdir -p $PLUGINS_ZSH
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${PLUGINS_ZSH}/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${PLUGINS_ZSH}/zsh-autosuggestions


force-link-config "zshrc" "$LINKED/.zshrc" "$HOME/.zshrc"

force-link-config "vimrc" "$LINKED/nvim/init.vim" "$HOME/.vimrc"
echo "install vim plugin..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
which vim && vim -c "PlugInstall" -c "exit" -c "exit"

nvim=$(which nvim)
if [ ! $? ]; then
	force-link-config "neovim" "$LINKED/nvim" "$HOME/.config/nvim"
	echo "install neovim plugin..."
	nvim -c "PlugInstall" -c "exit" -c "exit"
fi


force-link-config "scripts" "$LINKED/scripts" $SCRIPTS_PATH
chmod +x -R $SCRIPTS_PATH


force-link-config "docker-debug" "$LINKED/config.toml" "$HOME/.docker-debug/config.toml"
force-link-config "docker-extras" "$LINKED/docker-extras.py" "$BASE_DIR_PATH/docker-extras.py"
force-link-config "keepassxc" "$LINKED/keepassxc" "$HOME/.config/keepassxc"
force-link-config "terminator" "$LINKED/terminator" "$HOME/.config/terminator"
force-link-config "ulauncher" "$LINKED/ulauncher" "$HOME/.config/ulauncher"
force-link-config "valgrind" "$LINKED/valgrind" "$HOME/.config/valgrind"
force-link-config "kritadisplayrc" "$LINKED/kritadisplayrc" "$HOME/.config/kritadisplayrc"
force-link-config "kritarc" "$LINKED/kritarc" "$HOME/.config/kritarc"
force-link-config "yarn" "$LINKED/yarn/package.json" "$HOME/.config/yarn/global/package.json"
force-link-config "yarn.lock" "$LINKED/yarn/yarn.lock" "$HOME/.config/yarn/global/yarn.lock"
force-link-config ".ruff.toml" "$LINKED/.ruff.toml" "$HOME/.ruff.toml"
force-link-config ".nvmrc" "$LINKED/.nvmrc" "$HOME/.nvmrc"

echo "pypi config..."
cp -r "$LINKED/.pypirc" "$HOME/.pypirc"

echo "chmod ssh..."
mkdir -p ~/.ssh ~/.cache/ssh
chmod 700 ~/.ssh ~/.cache/ssh

echo "generate ssh key..."
ssh-keygen -f "$HOME/.ssh/github" -q -N ""
ssh-keygen -f "$HOME/.ssh/gitlab" -q -N ""

force-link-config "git" "$LINKED/.gitconfig" "$HOME/.gitconfig"

install_vscode_extentions () {
	exts=$(sed 's/^/ --install-extension /' "$LINKED/codium/extensions.txt" | tr '\n' ' ')
	$1 --force $exts
}

code=$(which codium)
if [ $? ]; then
	force-link-config "codium" "$LINKED/codium/User" "$HOME/.config/VSCodium/User"
	force-link-config "codium profil" "$LINKED/codium/product.json" "$HOME/.config/VSCodium/product.json"
	install_vscode_extentions codium
fi

code=$(which code)
if [ $? && $code != $(which codium) ]; then
	force-link-config "vscode" "$LINKED/codium/User" "$HOME/.config/Code/User"
	force-link-config "codium profil" "$LINKED/codium/product.json" "$HOME/.config/Code/product.json"
	install_vscode_extentions code
fi

echo "load dconf..."
dconf load / <"$LINKED/dconf.ini"