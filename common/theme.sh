#!/bin/bash


cd $(dirname $0)
color_blue="./linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue install themes)]"

echo "install ulauncher themes..."
ULAUN_PATH="$HOME/.config/ulauncher"
mkdir -p "$ULAUN_PATH/user-themes"
git clone https://github.com/fsrocha-dev/ulauncher-palenight-theme.git "$ULAUN_PATH/user-themes/palenight"

echo "install system themes..."
to="/tmp/Layan-gtk-theme"
rm -rf $to
git clone https://github.com/vinceliuice/Layan-gtk-theme.git $to
chmod +x "$to/install.sh"
"$to/install.sh"

to="/tmp/Tela-icon-themes"
rm -rf $to
git clone https://github.com/vinceliuice/Tela-icon-theme.git $to
chmod +x "$to/install.sh"
"$to/install.sh"

theme="Layan-dark"
if [[ $DESKTOP_SESSION == "gnome" ]]; then
    gsettings set org.gnome.desktop.interface gtk-theme $theme
    gsettings set org.gnome.desktop.wm.preferences theme $theme
    gsettings set org.gnome.desktop.interface icon-theme 'Tela-dark'
fi

echo "install jetbrains Mono fonts..."
fonts=JetBrainsMono-2.304.zip
wget https://download-cdn.jetbrains.com/fonts/$fonts -O /tmp/jetbrains.zip
unzip "/tmp/jetbrains.zip" -d /tmp/jetbrains
mv /tmp/jetbrains/fonts/ttf/*.ttf ~/.local/share/fonts/
rm -rf /tmp/jetbrains
fc-cache -vf

echo "thumbnailers..."
sudo echo """
[Thumbnailer Entry]
Exec=/usr/bin/ufraw-batch --embedded-image --out-type=png --size=%s %u --overwrite --silent --output=%o
MimeType=image/x-3fr;image/x-adobe-dng;image/x-arw;image/x-bay;image/x-canon-cr2;image/x-canon-crw;image/x-cap;image/x-cr2;image/x-crw;image/x-dcr;image/x-dcraw;image/x-dcs;image/x-dng;image/x-drf;image/x-eip;image/x-erf;image/x-fff;image/x-fuji-raf;image/x-iiq;image/x-k25;image/x-kdc;image/x-mef;image/x-minolta-mrw;image/x-mos;image/x-mrw;image/x-nef;image/x-nikon-nef;image/x-nrw;image/x-olympus-orf;image/x-orf;image/x-panasonic-raw;image/x-pef;image/x-pentax-pef;image/x-ptx;image/x-pxn;image/x-r3d;image/x-raf;image/x-raw;image/x-rw2;image/x-rwl;image/x-rwz;image/x-sigma-x3f;image/x-sony-arw;image/x-sony-sr2;image/x-sony-srf;image/x-sr2;image/x-srf;image/x-x3f;
""" >  /usr/share/thumbnailers/ufraw.thumbnailer
