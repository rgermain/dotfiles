#!/bin/bash

cd $(dirname $0)
color_blue="linked/scripts/dev/color/color-blue"

echo -e "\n[$($color_blue optimize gnome)]"

INSTALL=$1
REMOVE=$2

app=(
	abrt*
	libvirt*
	cheese
	totem totem-plugins
	gnome-weather
	gnome-photos
	simple-scan
	gnome-logs
	gnome-weather
	seahorse
	yelp
	gnome-terminal-nautilus
	gnome-tour
	gnome-system-monitor
	gnome-connections
	gnome-shell-extension-desktop-icons
	gnome-sudoku
	gnome-startup-applications
	gnome-todo
	gnome-mahjongg
	gnome-maps
	rhythmbox
	eog
)
echo "remove unnecessary gnome app..."

for app in ${app[@]}; do
	sudo $REMOVE $app
done

#PackageKit gnome-software

app=(
	gnome-tweak-tool
	breeze-cursor-theme
	papirus-icon-theme
	wmctrl
	nautilus-python
	gnome-extensions-cli
)
echo "install gnome app..."
sudo $INSTALL ${app[@]}
mkdir -p $HOME/.themes

# settings gnome

libuser=(
    nautilus-open-any-terminal
	gnome-extensions-cli
)

echo "install python pkgs..."
python3 -m pip install --user "${libuser[@]}"

glib-compile-schemas ~/.local/share/glib-2.0/schemas/
gsettings set org.gnome.shell.app-switcher current-workspace-only true
gsettings set com.github.stunkymonkey.nautilus-open-any-terminal terminal terminator
gsettings set com.github.stunkymonkey.nautilus-open-any-terminal new-tab false

echo "install gnome extensions..."
gext install $(cat ./linked/gnome_extensions.txt)